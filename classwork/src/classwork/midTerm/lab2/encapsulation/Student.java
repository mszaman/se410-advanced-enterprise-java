package classwork.midTerm.lab2.encapsulation;

public class Student {

    private long stdId; // declare instance variable for Student Id
    private String stdName; // declare instance variable for Student Name
    private double stdCgpa; // declare instance variable for Student CGPA

    // getter method for Student Id
    public long getStdId() {
        return stdId;
    }

    // setter method for Student Id
    public void setStdId(long stdId) {
        this.stdId = stdId;
    }

    // getter method for Student Name
    public String getStdName() {
        return stdName;
    }

    // setter method for Student Name
    public void setStdName(String stdName) {
        this.stdName = stdName;
    }

    // getter method for Student CGPA
    public double getStdCgpa() {
        return stdCgpa;
    }

    // setter method for Student CGPA
    public void setStdCgpa(double stdCgpa) {
        this.stdCgpa = stdCgpa;
    }

    // toString method for print Student information
    @Override
    public String toString() {
        return "Student{" +
                "stdId=" + stdId +
                ", stdName='" + stdName + '\'' +
                ", stdCgpa=" + stdCgpa +
                '}';
    }
}
