package classwork.midTerm.lab3.abstraction.usingInterfaceClass;

public interface Bank {
    public double getInterest();
}
