package lectureslide.midTerm.lab3.aggregation;

public class AggregationTest {
    public static void main(String[] args) {

        // Address object
        Address home = new Address();
        home.setId(1);
        home.setLine("road # 12");
        home.setCity("dhaka");
        home.setCountry("Bangladesh");

        // Employee object
        Employee employee = new Employee();
        employee.setEmpId(100);
        employee.setEmpName("zaman");
        employee.setAddress(home);
        employee.setSalary(10000);

        System.out.println(employee);

        employee = null;

        System.out.println(home);
    }
}
