package lectureslide.finalTerm.lab5.usingSwing;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class TableTest extends JFrame {
    private JPanel rootPanel;
    private JScrollPane sp;
    private JTable jt;

    public TableTest(){
        createUIComponents();
    }

    private void createUIComponents() {
        rootPanel = new JPanel();
        sp = new JScrollPane();
        jt = new JTable();

        jt.setModel(new DefaultTableModel(
                new Object[][]{
                        {null, null, null},
                        {null, null, null},
                        {null, null, null}
                },
                new String[] {
                        "id", "name", "subject"
                }
        ));
        sp.setViewportView(jt);

    }

    public static void main(String[] args) {
        new TableTest().setVisible(true);
    }
}
