package lectureslide.finalTerm.lab5.usingJavaFX.model;

import java.sql.*;

public class DatabaseConnection {
    public static Connection getConnection(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/cu","root","root");
            return con;
        }catch (Exception e){
            System.out.println(e);
            return null;
        }
    }
}
