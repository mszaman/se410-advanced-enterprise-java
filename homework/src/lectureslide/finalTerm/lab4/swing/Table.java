package lectureslide.finalTerm.lab4.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Table {

    JFrame frame;

    Table(){
        frame = new JFrame();
        frame.setSize(500, 500);

        Object[] columnNames = {"ID", "Name", "CGPA"};
        Object[][] data = {
                {"101", "sifat", "3.60"},
                {"102", "Pikul", "3.50"},
                {"103", "siraj", "3.40"}
        };

        JTable table = new JTable(data, columnNames);
        table.setBounds(50, 50, 300, 300);
        JScrollPane scrollPane = new JScrollPane(table);
        frame.add(scrollPane);

        frame.setLayout(new FlowLayout());
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.dispose();
            }
        });
    }

    public static void main(String[] args) {
        new Table();
    }
}
