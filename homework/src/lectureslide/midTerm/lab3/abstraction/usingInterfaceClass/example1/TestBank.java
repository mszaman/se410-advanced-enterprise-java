package lectureslide.midTerm.lab3.abstraction.usingInterfaceClass.example1;

public class TestBank {
    public static void main(String[] args) {
        Bank bank = new BRAC();
        System.out.println(bank.getInterest());

        Bank bank1 = new HSBC();
        System.out.println(bank1.getInterest());
    }
}
