package lectureslide.midTerm.lab3.abstraction.usingInterfaceClass.example3;

public class TestEmployee {
    public static void main(String[] args) {
        Employee employee = new Contractor();
        Employee employee1 = new FullTimeEmployee();

        System.out.println("The salary of contractor is : " + employee.calculateSalary());
        System.out.println("The salary of full time employee is : " + employee1.calculateSalary());
    }
}
