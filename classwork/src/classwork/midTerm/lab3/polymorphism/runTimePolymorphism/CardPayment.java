package classwork.midTerm.lab3.polymorphism.runTimePolymorphism;

public class CardPayment implements Payment {
    @Override
    public void payment(double amount) {
        System.out.println("tk - " + amount + " is paid by Card.");
    }
}
