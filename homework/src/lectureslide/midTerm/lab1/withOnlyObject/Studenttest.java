/*
* Written by:   MS Zaman
* Student ID:   141352032
* Department:   Computer Science & Engineering
* Varsity:      City University
* */

package lectureslide.midTerm.lab1.withOnlyObject;

// print student's information using only object
public class Studenttest {
    public static void main(String[] args) {

        int id; // declare ID of student
        String name; // declare Name of student
        double cgpa; // declare CGPA of student

        id = 141352032; // assign ID value into "id"
        name = "MS Zaman"; // assign Name value into "name"
        cgpa = 2.58; // assign CGPA value into "cgpa"

        // creating a "student" object under "Student" Class
        Student student = new Student(id, name, cgpa);

        System.out.println("this prints student's information using only object");
        System.out.println("---------------------------------------");
        System.out.println("My name is: " + student.name);
        System.out.println("My ID is: " + student.id);
        System.out.println("and my CGPA is: " + student.cgpa);
    }
}
