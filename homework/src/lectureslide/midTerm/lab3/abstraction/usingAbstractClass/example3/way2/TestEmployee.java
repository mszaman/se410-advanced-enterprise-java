package lectureslide.midTerm.lab3.abstraction.usingAbstractClass.example3.way2;

public class TestEmployee {
    public static void main(String[] args) {
        Employee employee = new Contractor();
        Employee employee1 = new FullTimeEmployee();

        employee.setSalary(2000);
        employee1.setSalary(1500);

        System.out.println("The salary of contractor is : " + employee.calculateSalary());
        System.out.println("The salary of full time employee is : " + employee1.calculateSalary());
    }
}
