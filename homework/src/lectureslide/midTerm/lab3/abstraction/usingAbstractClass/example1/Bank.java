package lectureslide.midTerm.lab3.abstraction.usingAbstractClass.example1;

public abstract class Bank {
    public abstract double getInterest();
}
