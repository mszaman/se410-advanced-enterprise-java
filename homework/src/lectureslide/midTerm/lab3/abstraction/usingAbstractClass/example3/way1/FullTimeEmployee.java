package lectureslide.midTerm.lab3.abstraction.usingAbstractClass.example3.way1;

public class FullTimeEmployee extends Employee{
    @Override
    public double calculateSalary() {
        return 1500;
    }
}
