package lectureslide.midTerm.lab3.abstraction.usingAbstractClass.example2.way1;

public class App {
    public static void main(String[] args) {
        MyTest myTest = new Addition();
        MyTest myTest1 = new Subtraction();
        MyTest myTest2 = new Multiplication();

        System.out.println("The summation is : " + myTest.calculate(15,5));
        System.out.println("The subtraction is : " + myTest1.calculate(20,2));
        System.out.println("The multiplication is : " + myTest2.calculate(10,20));
    }
}
