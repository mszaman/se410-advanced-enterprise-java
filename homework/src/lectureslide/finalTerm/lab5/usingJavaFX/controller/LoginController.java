package lectureslide.finalTerm.lab5.usingJavaFX.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import lectureslide.finalTerm.lab5.usingJavaFX.model.LoginModel;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class LoginController implements Initializable {
    public Label notilabel;
    public Button loginButton;
    public TextField loginUserTextField;
    public ChoiceBox loginPostChoiceBox;
    public PasswordField loginPassTextField;

    LoginModel lm = new LoginModel();


    public void loginButtonPushed(ActionEvent event) throws IOException {

        Parent dashboardParent = FXMLLoader.load(getClass().getResource("dashboard.fxml"));
        Scene dashboardScene = new Scene(dashboardParent);

        if (loginPostChoiceBox.getValue()=="admin"){

            try {
                if (lm.isAdminLogin(loginUserTextField.getText(), loginPassTextField.getText())){

                    Stage dashboardWindow = (Stage) ((Node)event.getSource()).getScene().getWindow();
                    dashboardWindow.setTitle("City University - Dashboard");
                    dashboardWindow.setScene(dashboardScene);
                    dashboardWindow.setResizable(false);
                    dashboardWindow.show();
                }
                else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Log In");
                    alert.setContentText("Invalid Information");
                    alert.setHeaderText(null);
                    alert.showAndWait();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (loginPostChoiceBox.getValue()=="student"){

            try {
                if (lm.isStudentLogin(loginUserTextField.getText(), loginPassTextField.getText())){
                    Stage dashboardWindow = (Stage) ((Node)event.getSource()).getScene().getWindow();
                    dashboardWindow.setTitle("City University - Dashboard");
                    dashboardWindow.setScene(dashboardScene);
                    dashboardWindow.setResizable(false);
                    dashboardWindow.show();
                }
                else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Log In");
                    alert.setContentText("Invalid Information");
                    alert.setHeaderText(null);
                    alert.showAndWait();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loginPostChoiceBox.getItems().addAll("admin","student","teacher");
        loginPostChoiceBox.setValue("admin");
    }
}
