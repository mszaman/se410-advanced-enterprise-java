package lectureslide.midTerm.lab3.abstraction.usingAbstractClass.example1;

public class abstractionTest {
    public static void main(String[] args) {
        Bank bank = new HSBC();
        Bank bank1 = new BRAC();

        System.out.println("The interest of HSBC bank is : " + bank.getInterest() + " %");
        System.out.println("The interest of BRAC bank is : " + bank1.getInterest() + " %");
    }
}
