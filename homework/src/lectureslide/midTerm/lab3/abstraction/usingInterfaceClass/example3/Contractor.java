package lectureslide.midTerm.lab3.abstraction.usingInterfaceClass.example3;

public class Contractor implements Employee {
    @Override
    public double calculateSalary() {
        return 1000;
    }
}
