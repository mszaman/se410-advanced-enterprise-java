package classwork.midTerm.lab3.polymorphism.compileTimePolymorphism;

public class CompileTimePolymorphismTest {
    public static void main(String[] args) {
        Payment payment = new Payment();

        payment.showPayment();

        payment.showPayment(123);
    }
}
