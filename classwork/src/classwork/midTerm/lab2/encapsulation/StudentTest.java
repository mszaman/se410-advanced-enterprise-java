package classwork.midTerm.lab2.encapsulation;

public class StudentTest {
    public static void main(String[] args) {

        // create "studentOne" object under "Student" class
        Student studentOne = new Student();

        studentOne.setStdId(1); // input Student One ID through setter method with the help of "studentOne" object
        studentOne.setStdName("MS Zaman"); // input Student One Name through setter method with the help of "studentOne" object
        studentOne.setStdCgpa(2.58); // input Student One CGPA through setter method with the help of "studentOne" object

        // print the Student One information which is in "studentOne" object"
        System.out.println(studentOne);

        // create "studentTwo" object under "Student" class
        Student studentTwo = new Student();

        studentTwo.setStdId(2); // input Student Two ID through setter method with the help of "studentTwo" object
        studentTwo.setStdName("Sifat Hossain"); // input Student Two Name through setter method with the help of "studentTwo" object
        studentTwo.setStdCgpa(3.06); // input Student Two CGPA through setter method with the help of "studentTwo" object

        // print the Student Two information which is in "studentTwo" object"
        System.out.println(studentTwo);
    }
}
