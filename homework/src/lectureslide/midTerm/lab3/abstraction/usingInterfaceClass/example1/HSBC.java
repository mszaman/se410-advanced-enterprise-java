package lectureslide.midTerm.lab3.abstraction.usingInterfaceClass.example1;

public class HSBC implements Bank {
    @Override
    public double getInterest() {
        return 10;
    }
}
