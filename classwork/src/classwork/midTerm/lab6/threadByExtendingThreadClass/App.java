package classwork.midTerm.lab6.threadByExtendingThreadClass;

public class App {
    public static void main(String[] args) {
        // way1 test
        MyClass1 c1 = new MyClass1();
        //c1.start();

        MyClass1 c2 = new MyClass1();
        //c2.start();

        Thread thread = new MyClass1();
        Thread thread1 = new MyClass1();
        Thread thread2 = new MyClass1();

        MyClass1 c3 = new MyClass1();
        MyClass1 c4 = new MyClass1();

        c1.start();
        c2.start();
        c3.start();
        c4.start();
        thread.start();
        thread1.start();
        thread2.start();
    }
}
