package lectureslide.finalTerm.lab3.simpleButton;

import java.awt.*;

public class ButtonTest {
    Frame f;
    ButtonTest(){
        f = new Frame("City University UI");
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);

        Button button = new Button("click me");
        button.setBounds(30, 100, 80, 20);
        f.add(button);
    }

    public static void main(String[] args) {
        ButtonTest cityUniUI = new ButtonTest();
    }
}
