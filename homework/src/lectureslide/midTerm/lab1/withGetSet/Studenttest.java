/*
 * Written by:   MS Zaman
 * Student ID:   141352032
 * Department:   Computer Science & Engineering
 * Varsity:      City University
 * */

package lectureslide.midTerm.lab1.withGetSet;

import lectureslide.midTerm.lab1.Student;

// print student's information using set and get method
public class Studenttest {
    public static void main(String[] args) {

        int id; // declare ID of student
        String name; // declare Name of student
        double cgpa; // declare CGPA of student

        id = 141352032; // assign ID value into "id"
        name = "MS Zaman"; // assign Name value into "name"
        cgpa = 2.58; // assign CGPA value into "cgpa"

        // creating a "student" object under "Student" Class
        Student student = new Student();

        student.setId(id); // setting the student's ID into "Student" Class through "student" object
        student.setName(name); // setting the student's Name into "Student" Class through "student" object
        student.setCgpa(cgpa); // setting the student's CGPA into "Student" Class through "student" object

        System.out.println("this prints student's information using set and get methods");
        System.out.println("---------------------------------------");

        // getting student's Name from "Student" Class through "student" object and print it
        System.out.println("My name is: " + student.getName());

        // getting student's ID from "Student" Class through "student" object and print it
        System.out.println("My ID number is: " + student.getId());

        // getting student's CGPA from "Student" Class through "student" object and print it
        System.out.println("and my CGPA is: " + student.getCgpa());
    }
}
