package classwork.midTerm.lab3.polymorphism.runTimePolymorphism;

public interface Payment {
    public void payment(double amount);
}
