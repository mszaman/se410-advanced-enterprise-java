package lectureslide.midTerm.lab3.abstraction.usingAbstractClass.example2.way1;

public class Addition extends MyTest {

    @Override
    public int calculate(int firstNumber, int secondNumber) {
        return firstNumber+secondNumber;
    }
}
