package lectureslide.finalTerm.lab5.usingSwing;

import lectureslide.finalTerm.lab5.usingSwing.controller.Student;

public class App {

    public static void main(String[] args) throws Exception {
        Student student = new Student();
        student.setTitle("Student Information");
        student.setVisible(true);
    }
}
