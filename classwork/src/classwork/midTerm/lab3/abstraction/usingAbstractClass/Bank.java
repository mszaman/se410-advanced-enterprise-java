package classwork.midTerm.lab3.abstraction.usingAbstractClass;

public abstract class Bank {
    public abstract double getInterest();
}
