package lectureslide.midTerm.lab1;

public class Student {
    private int id; // instance variable for student's ID
    private String name; // instance variable for student's Name
    private double cgpa; // instance variable for student's CGPA

    // method for setting the student's ID
    public void setId(int id){
        this.id = id;
    }
    // method for getting the student's ID
    public int getId(){
        return id;
    }

    // method for setting the student's Name
    public void setName(String name){
        this.name = name;
    }
    // method for getting the student's Name
    public String getName(){
        return name;
    }

    // method for setting the student's CGPA
    public void setCgpa(double cgpa){
        this.cgpa = cgpa;
    }
    // method for getting student's CGPA
    public double getCgpa(){
        return cgpa;
    }

    public String toString(){
        return "My name is: " + name + "\nMy ID is: " + id + "\nand my CGPA is: " + cgpa;
    }
}
