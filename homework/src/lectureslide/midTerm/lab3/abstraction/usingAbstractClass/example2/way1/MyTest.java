package lectureslide.midTerm.lab3.abstraction.usingAbstractClass.example2.way1;

public abstract class MyTest {
    public abstract int calculate(int firstNumber, int secondNumber);
}
