package classwork.midTerm.lab3.abstraction.usingInterfaceClass;

public class HSBC implements Bank {
    @Override
    public double getInterest() {
        return 10;
    }
}
