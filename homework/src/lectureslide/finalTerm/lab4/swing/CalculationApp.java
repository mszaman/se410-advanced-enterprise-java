package lectureslide.finalTerm.lab4.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CalculationApp {
    JFrame frame;
    JButton buttonAddition, buttonSubtraction, buttonMultiplication, buttonDivision, buttonReset;
    JTextField textFieldFirst, textFieldSecond, textFieldResult;
    CalculationApp(){

        frame = new JFrame("My Calculator");
        frame.setSize(500, 150);
        frame.setLayout(new FlowLayout());
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        textFieldFirst = new JTextField(10);
        textFieldSecond = new JTextField(10);
        textFieldResult = new JTextField( 21);
        buttonAddition = new JButton("+");
        buttonSubtraction = new JButton("-");
        buttonMultiplication = new JButton("*");
        buttonDivision = new JButton("/");
        buttonReset = new JButton("reset");

        frame.add(textFieldFirst);
        frame.add(textFieldSecond);
        frame.add(textFieldResult);
        frame.add(buttonAddition);
        frame.add(buttonSubtraction);
        frame.add(buttonMultiplication);
        frame.add(buttonDivision);
        frame.add(buttonReset);

        buttonAddition.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int firstNumber = Integer.parseInt(textFieldFirst.getText());
                int secondNumber = Integer.parseInt(textFieldSecond.getText());
                textFieldResult.setText(Integer.toString(firstNumber+secondNumber));
            }
        });
        buttonSubtraction.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int firstNumber = Integer.parseInt(textFieldFirst.getText());
                int secondNumber = Integer.parseInt(textFieldSecond.getText());
                textFieldResult.setText(String.valueOf(firstNumber-secondNumber));
            }
        });
        buttonMultiplication.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int firstNumber = Integer.parseInt(textFieldFirst.getText());
                int secondNumber = Integer.parseInt(textFieldSecond.getText());
                textFieldResult.setText(String.valueOf(firstNumber*secondNumber));
            }
        });
        buttonDivision.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int firstNumber = Integer.parseInt(textFieldFirst.getText());
                int secondNumber = Integer.parseInt(textFieldSecond.getText());
                textFieldResult.setText(String.valueOf(firstNumber/secondNumber));
            }
        });
        buttonReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                textFieldFirst.setText("");
                textFieldSecond.setText("");
                textFieldResult.setText("");
            }
        });

    }

    public static void main(String[] args) {

        new CalculationApp();
    }
}
