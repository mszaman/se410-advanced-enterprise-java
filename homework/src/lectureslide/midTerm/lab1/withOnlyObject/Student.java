package lectureslide.midTerm.lab1.withOnlyObject;

public class Student {
    public int id; // instance variable for student's ID
    public String name; // instance variable for student's Name
    public double cgpa; // instance variable for student's CGPA

    public Student(int id, String name, double cgpa) {
        this.id = id;
        this.name = name;
        this.cgpa = cgpa;
    }
}
