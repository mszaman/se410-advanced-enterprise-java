package lectureslide.finalTerm.lab5.usingJavaFX.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentModel {
    Connection con;
    private String studentName;
    private String studentUserName;
    private String studentPassword;

    //ObservableList<StudentModelTable> oblist = FXCollections.observableArrayList();

    private String insertionQuery = "insert into student(studentName,studentUserName,studentPassword) values(?,?,?)";

    public StudentModel(){
        con = DatabaseConnection.getConnection();
    }

    public boolean insertStudent(){
        try {
            PreparedStatement ps = con.prepareStatement(insertionQuery);
            ps.setString(1,studentName);
            ps.setString(2,studentUserName);
            ps.setString(3,studentPassword);
            ps.executeUpdate();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
/*
    public ObservableList<StudentModelTable> fetchStudent(){
        try {
            ResultSet rs = con.createStatement().executeQuery("select * from student");
            while (rs.next()){
                oblist.add(new StudentModelTable(Integer.toString(rs.getInt(1)), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return oblist;
    }*/

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public void setStudentUserName(String studentUserName) {
        this.studentUserName = studentUserName;
    }

    public void setStudentPassword(String studentPassword) {
        this.studentPassword = studentPassword;
    }
}
