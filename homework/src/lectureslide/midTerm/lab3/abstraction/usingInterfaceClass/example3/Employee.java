package lectureslide.midTerm.lab3.abstraction.usingInterfaceClass.example3;

public interface Employee {
    public double calculateSalary();
}
