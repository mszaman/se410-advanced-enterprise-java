package lectureslide.midTerm.lab3.abstraction.usingInterfaceClass.example3;

public class FullTimeEmployee implements Employee {
    @Override
    public double calculateSalary() {
        return 1500;
    }
}
