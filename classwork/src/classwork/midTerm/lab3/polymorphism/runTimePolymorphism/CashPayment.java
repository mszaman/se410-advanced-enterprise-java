package classwork.midTerm.lab3.polymorphism.runTimePolymorphism;

public class CashPayment implements Payment {
    @Override
    public void payment(double amount){
        System.out.println("tk - "+ amount + " is paid by Cash.");
    }
}
