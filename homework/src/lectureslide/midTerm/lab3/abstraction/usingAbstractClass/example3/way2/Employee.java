package lectureslide.midTerm.lab3.abstraction.usingAbstractClass.example3.way2;

public abstract class Employee {
    private double salary;

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public abstract double calculateSalary();
}
