package classwork.midTerm.lab6.threadByImplementingRunnableInterface;

public class MyClass2 implements Runnable {
    @Override
    public void run(){
        for(int i=0; i<5; i++){
            System.out.println("Thread is " + Thread.currentThread().getId() + " and the Value is " + i);
        }
    }
}
