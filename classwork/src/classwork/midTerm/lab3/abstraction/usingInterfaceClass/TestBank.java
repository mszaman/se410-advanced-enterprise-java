package classwork.midTerm.lab3.abstraction.usingInterfaceClass;

public class TestBank {
    public static void main(String[] args) {
        Bank bank = new BRAC();
        System.out.println(bank.getInterest());

        Bank bank1 = new HSBC();
        System.out.println(bank1.getInterest());
    }
}
