package lectureslide.finalTerm.lab5.usingJavaFX.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import lectureslide.finalTerm.lab5.usingJavaFX.model.DatabaseConnection;
import lectureslide.finalTerm.lab5.usingJavaFX.model.StudentModel;
import lectureslide.finalTerm.lab5.usingJavaFX.model.StudentModelTable;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class DashboardController implements Initializable {

    public Button logoutButton;
    public TextField studentNameTextField;
    public TextField studentUserNameTextField;
    public TextField studentPasswordTextField;
    public Button resetButton;
    public Button addStudentButton;
    public Button searchButton;
    public Button updateButton;
    public Button deleteButton;
    public Button allStudentButton;
    public TableView<StudentModelTable> studentTable;
    public TableColumn<StudentModelTable, String> nameColumn;
    public TableColumn<StudentModelTable, String> idColumn;
    public TableColumn<StudentModelTable, String> userNameColumn;
    public TableColumn<StudentModelTable, String> passwordColumn;

    ObservableList<StudentModelTable> oblist;

    Connection con;


    StudentModel studentModel = new StudentModel();

    public void logoutButtonPushed(ActionEvent event) throws IOException {
        Parent loginParent = FXMLLoader.load(getClass().getResource("login.fxml"));
        Scene loginScene = new Scene(loginParent);
        Stage loginWindow = (Stage) ((Node)event.getSource()).getScene().getWindow();
        loginWindow.setTitle("City University - Login");
        loginWindow.setScene(loginScene);
        loginWindow.show();
    }

    public void resetButtonPushed(ActionEvent event) throws IOException {
        studentNameTextField.setText("");
        studentUserNameTextField.setText("");
        studentPasswordTextField.setText("");
    }

    public void addStudentButtonPushed(ActionEvent event) {
        String studentName = studentNameTextField.getText();
        String studentUserName = studentUserNameTextField.getText();
        String studentPassword = studentPasswordTextField.getText();
        boolean confirmation;

        studentModel.setStudentName(studentName);
        studentModel.setStudentUserName(studentUserName);
        studentModel.setStudentPassword(studentPassword);

        if (studentModel.insertStudent()){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Insert Student");
            alert.setContentText("'" + studentName + "' is successfully inserted into database");
            alert.setHeaderText(null);
            alert.showAndWait();

            studentNameTextField.setText("");
            studentUserNameTextField.setText("");
            studentPasswordTextField.setText("");
        }
        else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Insert Student");
            alert.setContentText("Some problem occurs");
            alert.setHeaderText(null);
            alert.showAndWait();
        }
    }

    public void searchButtonPushed(ActionEvent event) {
    }

    public void updateButtonPushed(ActionEvent event) {
    }

    public void deleteButtonPushed(ActionEvent event) {
    }

    public void allStudentButtonPushed(ActionEvent event) {
        try {
            oblist = FXCollections.observableArrayList();
            ResultSet rs = con.createStatement().executeQuery("select * from student order by studentId desc");
            while (rs.next()){
                oblist.add(new StudentModelTable(Integer.toString(rs.getInt(1)), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        idColumn.setCellValueFactory(new  PropertyValueFactory<>("id"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        userNameColumn.setCellValueFactory(new PropertyValueFactory<>("uname"));
        passwordColumn.setCellValueFactory(new PropertyValueFactory<>("pass"));
        studentTable.setItems(null);
        studentTable.setItems(oblist);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        /*idColumn.setCellValueFactory(new  PropertyValueFactory<>("id"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        userNameColumn.setCellValueFactory(new PropertyValueFactory<>("uname"));
        passwordColumn.setCellValueFactory(new PropertyValueFactory<>("pass"));
        studentTable.setItems(studentModel.fetchStudent());*/

        con = DatabaseConnection.getConnection();
    }
}
