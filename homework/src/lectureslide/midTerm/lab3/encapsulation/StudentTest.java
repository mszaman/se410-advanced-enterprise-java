package lectureslide.midTerm.lab3.encapsulation;

public class StudentTest {
    public static void main(String[] args) {
        Student student = new Student();

        student.setName("MS Zaman");

        System.out.println("The name of the student is : "+student.getName());
    }
}
