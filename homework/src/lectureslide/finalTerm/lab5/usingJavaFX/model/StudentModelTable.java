package lectureslide.finalTerm.lab5.usingJavaFX.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StudentModelTable {
    private final StringProperty id;
    private final StringProperty name;
    private final StringProperty uname;
    private final StringProperty pass;

    public StudentModelTable(String id, String name, String uname, String pass){
        this.id = new SimpleStringProperty(id);
        this.name = new SimpleStringProperty(name);
        this.uname = new SimpleStringProperty(uname);
        this.pass = new SimpleStringProperty(pass);
    }

    public String getId(){
        return id.get();
    }

    public String getName(){
        return name.get();
    }

    public String getUname(){
        return uname.get();
    }

    public String getPass(){
        return pass.get();
    }

    public void setId(String s){
        id.set(s);
    }

    public void setName(String s){
        name.set(s);
    }

    public void setUname(String s){
        uname.set(s);
    }

    public void setPass(String s){
        pass.set(s);
    }

    public StringProperty idProperty(){
        return id;
    }

    public StringProperty nameProperty(){
        return name;
    }

    public StringProperty unameProperty(){
        return uname;
    }

    public StringProperty passProperty(){
        return pass;
    }
}
