package lectureslide.midTerm.lab3.abstraction.usingAbstractClass.example2.way2;

public class Subtraction extends MyTest {

    @Override
    public int calculate() {
        return getFirstNumber()-getSecondNumber();
    }
}
