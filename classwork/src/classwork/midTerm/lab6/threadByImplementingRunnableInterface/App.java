package classwork.midTerm.lab6.threadByImplementingRunnableInterface;

public class App {
    public static void main(String[] args) {
        MyClass2 c1 = new MyClass2();
        MyClass2 c2 = new MyClass2();

        Thread t1 = new Thread(c1);
        Thread t2 = new Thread(c2);

        Thread t3 = new Thread(new MyClass2());
        Thread t4 = new Thread(new MyClass2());

        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
