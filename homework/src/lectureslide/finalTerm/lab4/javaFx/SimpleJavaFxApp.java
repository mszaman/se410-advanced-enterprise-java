package lectureslide.finalTerm.lab4.javaFx;

import javafx.application.Application;
import javafx.stage.Stage;

public class SimpleJavaFxApp extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
