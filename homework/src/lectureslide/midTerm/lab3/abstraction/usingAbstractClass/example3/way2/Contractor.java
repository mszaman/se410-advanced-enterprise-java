package lectureslide.midTerm.lab3.abstraction.usingAbstractClass.example3.way2;

public class Contractor extends Employee {

    @Override
    public double calculateSalary() {
        return getSalary();
    }
}
