package lectureslide.midTerm.lab3.abstraction.usingAbstractClass.example3.way1;

public abstract class Employee {
    public abstract double calculateSalary();
}
