package lectureslide.finalTerm.lab4.swing;

import javax.swing.*;

public class SimpleSwingApp {
    JFrame frame;

    SimpleSwingApp(){
        frame = new JFrame("Simple Swing Application");
        frame.setSize(700,500);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        new SimpleSwingApp();
    }
}
