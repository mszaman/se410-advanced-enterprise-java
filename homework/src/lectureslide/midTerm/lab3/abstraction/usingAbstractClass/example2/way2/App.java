package lectureslide.midTerm.lab3.abstraction.usingAbstractClass.example2.way2;

public class App {
    public static void main(String[] args) {
        MyTest myTest = new Addition();
        myTest.setFirstNumber(10);
        myTest.setSecondNumber(5);

        MyTest myTest1 = new Subtraction();
        myTest1.setFirstNumber(20);
        myTest1.setSecondNumber(2);

        MyTest myTest2 = new  Multiplication();
        myTest2.setFirstNumber(30);
        myTest2.setSecondNumber(20);

        System.out.println("The summation is : " + myTest.calculate());
        System.out.println("The subtraction is : " + myTest1.calculate());
        System.out.println("The multiplication is : " + myTest2.calculate());
    }
}
