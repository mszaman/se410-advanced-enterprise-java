package lectureslide.midTerm.lab3.abstraction.usingInterfaceClass.example1;

public interface Bank {
    public double getInterest();
}
