package lectureslide.finalTerm.lab5.usingJavaFX.model;

import java.sql.*;

public class LoginModel {
    Connection con;

    public LoginModel(){
        con = DatabaseConnection.getConnection();
    }

    public boolean isAdminLogin(String userName, String password) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select * from admin where adminUsername=? and adminPassword=?";
        try {
            ps = con.prepareStatement(query);
            ps.setString(1,userName);
            ps.setString(2,password);
            rs = ps.executeQuery();

            if (rs.next())
                return true;
            else
                return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally {
            ps.close();
            rs.close();
        }
    }

    public boolean isStudentLogin(String userName, String password) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "select * from student where studentUserName=? and studentPassword=?";
        try {
            ps = con.prepareStatement(query);
            ps.setString(1,userName);
            ps.setString(2,password);
            rs = ps.executeQuery();

            if (rs.next())
                return true;
            else
                return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally {
            ps.close();
            rs.close();
        }
    }
}
