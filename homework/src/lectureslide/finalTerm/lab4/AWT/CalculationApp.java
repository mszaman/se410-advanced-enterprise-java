package lectureslide.finalTerm.lab4.AWT;

import java.awt.*;
import java.awt.event.*;

public class CalculationApp {
    Frame frame;
    Button buttonAddition, buttonSubtraction, buttonMultiplication, buttonDivision, buttonReset;
    TextField textFieldFirst, textFieldSecond, textFieldResult;
    CalculationApp(){

        frame = new Frame("My Calculator");
        frame.setSize(400, 200);
        frame.setLayout(new FlowLayout());
        frame.setVisible(true);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        textFieldFirst = new TextField(10);
        textFieldSecond = new TextField(10);
        textFieldResult = new TextField( 21);
        buttonAddition = new Button("+");
        buttonSubtraction = new Button("-");
        buttonMultiplication = new Button("*");
        buttonDivision = new Button("/");
        buttonReset = new Button("reset");

        frame.add(textFieldFirst);
        frame.add(textFieldSecond);
        frame.add(textFieldResult);
        frame.add(buttonAddition);
        frame.add(buttonSubtraction);
        frame.add(buttonMultiplication);
        frame.add(buttonDivision);
        frame.add(buttonReset);

        buttonAddition.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int firstNumber = Integer.parseInt(textFieldFirst.getText());
                int secondNumber = Integer.parseInt(textFieldSecond.getText());
                textFieldResult.setText(Integer.toString(firstNumber+secondNumber));
            }
        });
        buttonSubtraction.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int firstNumber = Integer.parseInt(textFieldFirst.getText());
                int secondNumber = Integer.parseInt(textFieldSecond.getText());
                textFieldResult.setText(String.valueOf(firstNumber-secondNumber));
            }
        });
        buttonMultiplication.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int firstNumber = Integer.parseInt(textFieldFirst.getText());
                int secondNumber = Integer.parseInt(textFieldSecond.getText());
                textFieldResult.setText(String.valueOf(firstNumber*secondNumber));
            }
        });
        buttonDivision.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int firstNumber = Integer.parseInt(textFieldFirst.getText());
                int secondNumber = Integer.parseInt(textFieldSecond.getText());
                textFieldResult.setText(String.valueOf(firstNumber/secondNumber));
            }
        });
        buttonReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                textFieldFirst.setText("");
                textFieldSecond.setText("");
                textFieldResult.setText("");
            }
        });

    }

    public static void main(String[] args) {
        new CalculationApp();
    }
}
