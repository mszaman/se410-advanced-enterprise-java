package lectureslide.finalTerm.lab4.javaFx;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.awt.*;

public class StageWithComponent extends Application {
    @Override
    public void start(Stage stage) throws Exception{
        stage.setTitle("My Java FX Title");
        Group group = new Group();

        Button button = new Button("Click me");
        button.setOnAction(event -> System.out.println("This is a javaFx Application."));


        group.getChildren().addAll(button);

        stage.setScene(new Scene(group, 400, 500));
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
