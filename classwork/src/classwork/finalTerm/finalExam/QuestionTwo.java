package classwork.finalTerm.finalExam;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class QuestionTwo {
    JFrame frame;
    JPanel panelField, panelTable;
    JLabel nameLabel, idLabel, genderLabel;
    JTextField nameTextField, idTextField;
    JRadioButton maleRadioButton, femaleRadioButton;
    JButton submitButton;
    JTable table;
    JScrollPane scrollPane;
    String gender="";

    QuestionTwo(){

        frame = new JFrame();
        frame.setSize(500, 900);

        panelField = new JPanel();
        panelTable = new JPanel();

        nameLabel = new JLabel("Name");
        idLabel = new JLabel("ID");
        genderLabel = new JLabel("Gender");
        nameTextField = new JTextField("");
        idTextField = new JTextField("");
        maleRadioButton = new JRadioButton("Male");
        femaleRadioButton = new JRadioButton("Female");
        submitButton = new JButton("Submit");
        table = new JTable();
        scrollPane = new JScrollPane(table);


        nameLabel.setBounds(50, 50, 50, 30);
        nameTextField.setBounds(50, 80, 100, 30);
        idLabel.setBounds(50, 120, 50, 30);
        idTextField.setBounds(50, 150, 100, 30);
        genderLabel.setBounds(50, 200, 50, 30);
        maleRadioButton.setBounds(50, 230, 100, 30);
        femaleRadioButton.setBounds(50, 260, 100, 30);
        submitButton.setBounds(50, 310, 100, 30);

        table.setModel(new DefaultTableModel(
                new Object[][]{

                },
                new String[]{
                        "Name", "ID", "Gender"
                }
        ));

        scrollPane.setViewportView(table);

        maleRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (maleRadioButton.isSelected()){
                    gender="Male";
                    maleRadioButton.setSelected(true);
                    femaleRadioButton.setSelected(false);
                }
            }
        });

        femaleRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (femaleRadioButton.isSelected()){
                   gender="Female";
                   maleRadioButton.setSelected(false);
                   femaleRadioButton.setSelected(true);
                }
            }
        });

        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DefaultTableModel model = (DefaultTableModel) table.getModel();
                model.addRow(new Object[]{nameTextField.getText(), idTextField.getText(), gender});

                nameTextField.setText("");
                idTextField.setText("");
                maleRadioButton.setSelected(false);
                femaleRadioButton.setSelected(false);
            }
        });

        frame.setVisible(true);
        panelField.add(nameLabel);
        panelField.add(nameTextField);
        panelField.add(idLabel);
        panelField.add(idTextField);
        panelField.add(genderLabel);
        panelField.add(maleRadioButton);
        panelField.add(femaleRadioButton);
        panelField.add(submitButton);
        panelTable.add(scrollPane);
        panelField.setLayout(null);
        frame.add(panelField);
        frame.add(panelTable,BorderLayout.SOUTH);

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.dispose();
            }
        });
    }

    public static void main(String[] args) {
        new QuestionTwo();
    }
}
